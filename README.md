# trtlooc.org

This is a child theme of WordPress' twentytwelve theme.

## Requirements

TODO: pull in `.htaccess` and `wp-config.php` into this directory.
`wp-config.php` contains too many things that should not be in a world-viewable
location. I'll need to rethink how I track changes to it.

In the theme folder, run the following commands:

    cd ../../..
    ln -s ./wp-content/themes/trtlooc/.htaccess .htaccess

    wp plugin install jetpack
    wp plugin install wp-meetup
    wp plugin install wp-optimize
    wp plugin install simple-facebook-connect
