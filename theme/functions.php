<?php
// little baby php file

function trtl_favicons() {
  $template_dir = get_stylesheet_directory_uri();
  echo <<<EOL
<link rel=icon type=image/png href={$template_dir}/images/favicon_64.png>
<link rel=apple-touch-icon type=image/png href={$template_dir}/images/favicon_57.png>
<link rel=apple-touch-icon type=image/png href={$template_dir}/images/favicon_114.png sizes=114x114>
<link rel=apple-touch-icon type=image/png href={$template_dir}/images/favicon_72.png sizes=72x72>
<link rel=apple-touch-icon type=image/png href={$template_dir}/images/favicon_144.png sizes=144x144>
<link rel='shortcut icon' href={$template_dir}/images/favicon.ico>
EOL;
}

add_action( 'wp_head', 'trtl_favicons' );
